cmake_minimum_required(VERSION 3.0.0)
project(zlib)

option(ZLIB_ENABLE_AMD64_ASM "Enable building amd64 assembly implementation" OFF)
option(ZLIB_ENABLE_I686_ASM "Enable building i686 assembly implementation" OFF)

set(AMD64 ${ZLIB_ENABLE_AMD64_ASM} CACHE BOOL "" FORCE)
set(ASM686 ${ZLIB_ENABLE_I686_ASM} CACHE BOOL "" FORCE)

configure_file(CMakeLists.txt.in zlib-download/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" . WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/zlib-download)
execute_process(COMMAND ${CMAKE_COMMAND} --build . WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/zlib-download)

add_subdirectory(${CMAKE_BINARY_DIR}/zlib-src ${CMAKE_BINARY_DIR}/zlib-build)