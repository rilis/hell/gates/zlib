## zlib proxy repository for hell

This is proxy repository for [zlib library](https://www.zlib.net), which allow you to build and install it using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of zlib using hell, have improvement idea, or want to request support for other versions of zlib, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in zlib itself please create issue on [zlib issue tracker](https://github.com/madler/zlib/issues), because here we don't do any kind of zlib development.
